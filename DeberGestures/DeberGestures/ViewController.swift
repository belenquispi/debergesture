//
//  ViewController.swift
//  DeberGestures
//
//  Created by Belen Quispi on 29/6/17.
//  Copyright © 2017 Belen Quispi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tapInputTextField: UITextField!
    
    
    @IBOutlet weak var numberLabel: UILabel!
    
    
    @IBOutlet weak var colorView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        numberLabel.text = "\(touch!.tapCount)"
        var cam  = Int ((touch?.tapCount)!)
        var ingreso = Int (tapInputTextField.text!)
        
        if (cam == ingreso)
        {
            colorView.backgroundColor = UIColor.red
        }
        
        
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
            colorView.backgroundColor = UIColor.orange
    }

}

